package dev.adafycheng.microservices.client.account;

import com.fasterxml.jackson.databind.ObjectMapper;
import dev.adafycheng.microservices.client.account.model.CustomerAccount;
import dev.adafycheng.microservices.client.account.mvc.CustomerAccountController;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
@TestPropertySource(
        locations = "classpath:application-integrationtest.properties")
class ApplicationTests {

    @Autowired
    private CustomerAccountController customerAccountController;

    @Autowired
    private MockMvc mockMvc;

    @Test
    void contextLoads() {
        assertThat(customerAccountController).isNotNull();
    }

    @Test
    void responseJsonString() throws Exception {
        String responseJsonString = mockMvc.perform(get("/api/customerAccounts"))
                .andDo(print())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ObjectMapper objectMapper = new ObjectMapper();

        //Convert to Java object in Jackson
        List<CustomerAccount> responseJson = objectMapper.readValue(responseJsonString,  List.class);

        //Create expected results with Java objects
        List<CustomerAccount> expected = new ArrayList<CustomerAccount>();

        //Comparison
        assertThat(responseJson).isNotNull();
    }

    /**
    @Test
    void findByLastName() throws Exception {
        mockMvc.perform(get("/api/customerAccount/Boy"))
                .andDo(print())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName", is("Handsome")));
    }
    */

}
