# You can override the included template(s) by including variable overrides
# SAST customization: https://docs.gitlab.com/ee/user/application_security/sast/#customizing-the-sast-settings
# Secret Detection customization: https://docs.gitlab.com/ee/user/application_security/secret_detection/#customizing-settings
# Dependency Scanning customization: https://docs.gitlab.com/ee/user/application_security/dependency_scanning/#customizing-the-dependency-scanning-settings
# Container Scanning customization: https://docs.gitlab.com/ee/user/application_security/container_scanning/#customizing-the-container-scanning-settings
# Note that environment variables can be set in several places
# See https://docs.gitlab.com/ee/ci/variables/#cicd-variable-precedence
# Code Quality: https://docs.gitlab.com/ee/user/project/merge_requests/code_quality.html

image: docker:latest
services:
  - docker:dind

stages:
- build
- test

include:
  #- template: Jobs/Build.gitlab-ci.yml
  - template: Code-Quality.gitlab-ci.yml
  - template: Security/Container-Scanning.gitlab-ci.yml
  - template: Security/Dependency-Scanning.gitlab-ci.yml
  - template: Security/SAST.gitlab-ci.yml
  - template: Security/SAST-IaC.latest.gitlab-ci.yml
  - template: Security/Secret-Detection.gitlab-ci.yml
  - template: Security/License-Scanning.gitlab-ci.yml

variables:
  SAST_JAVA_VERSION: 11

############
# Build
############
maven-build:
  image: maven:3.6-jdk-11-slim
  stage: build
  rules:
    - changes:
      - pom.xml
      - src/main/*
      - src/test/*
  script:
    - mvn package -Dmaven.repo.local=./.m2/repository -DskipTests
  artifacts:
    paths:
      - .m2/
      - target/

docker-build:
  stage: build
  rules:
    - changes:
      - pom.xml
      - src/main/*
      - src/test/*
      - Dockerfile
  script:
    - echo $CI_REGISTRY_IMAGE
    - echo $CI_DEFAULT_BRANCH
    - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $CI_REGISTRY_IMAGE
    - docker build -t $CI_REGISTRY_IMAGE/$CI_DEFAULT_BRANCH:$CI_COMMIT_SHA .
    - docker push $CI_REGISTRY_IMAGE/$CI_DEFAULT_BRANCH:$CI_COMMIT_SHA

############
# Test
############
semgrep:
  image: returntocorp/semgrep
  stage: test
  variables:
    # Never fail the build due to findings on pushes.
    # Instead, just collect findings for semgrep.dev/manage/findings
    SEMGREP_APP_TOKEN: $SEMGREP_APP_TOKEN
    SEMGREP_AUDIT_ON: push
  rules:
    - if: $CI_MERGE_REQUEST_IID
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  script: semgrep ci

snyk_dependency_scanning:
  image: maven:3.6-jdk-11-slim
  stage: test
  allow_failure: true
  script:
    - mvn install -DskipTests
    - curl -fsSL https://deb.nodesource.com/setup_current.x | bash -
    - apt-get install -y nodejs
    - apt-get upgrade -y
    - npm install -g npm@latest
    - npm install -g snyk
    - npm install snyk-to-html -g
    # Run snyk help, snyk auth, snyk monitor, snyk test to break build and out report
    - snyk --help
    - snyk auth $SNYK_TOKEN
    - snyk monitor --project-name=ms-client-account-gcp-gitlab
    - snyk test --json | snyk-to-html -o snyk_results.html
  # Save report to artifacts
  artifacts:
    when: always
    paths: 
      - snyk_results.html

license_scanning:
  stage: test
  variables:
    MAVEN_CLI_OPTS: -DskipTests --debug
    LM_JAVA_VERSION: 11
  allow_failure: true
  artifacts:
    paths:
      - gl-license-scanning-report.json
  #only:
  #  - branches
  #except:
  #  variables:
  #    - $LICENSE_MANAGEMENT_DISABLED

container_scanning:
  stage: test
  variables:
    CS_DEFAULT_BRANCH_IMAGE: $CI_REGISTRY_IMAGE/$CI_DEFAULT_BRANCH:$CI_COMMIT_SHA
    DOCKERFILE_PATH: Dockerfile
    GIT_STRATEGY: fetch
  rules:
    - changes:
      - pom.xml
      - src/main/*
      - src/test/*
      - Dockerfile
  needs: [docker-build]

