# Java Microservice in Google Cloud

>  This is a simple Spring Boot application which acts a microservice for managing Customer Account data in Google Firestore.

## Development

Refer to my article [Java Microservice on Google Kubernetes Engine (GKE) Cluster](https://blog.adafycheng.dev/java-microservice-on-google-kubernetes-engine-gke-cluster).

## Test the application functions locally

In IntelliJ Terminal,

1. List credentialed accounts and make sure the desired account is set active.

    ```shell
    gcloud auth list
    ```

2. List all projects.

    ```shell
    gcloud projects list
    ```

3. Set the default project.

    ```shell
    gcloud config set project PROJECT_ID
    ```

    * *Replace PROJECT_ID with your project ID.*

4. Show the configuration.

    ```shell
    gcloud config list
    ```

5. Start the Spring Boot application.

    ```shell
    ./mvnw spring-boot:run
    ```
    Use a REST client, like Postman, to perform following tests:

5. Perform health check.

   ![Health Check Local Test](images/20220117-LocalTest-HealthCheck.png)

6. Test application function: Create new Customer Account.

   ![Local Test: Create new Customer Account](images/20220117-LocalTest-NewCustomerAccount.png)

7. Test application function: Find by Last Name.

   ![Local Test: Find by Last Name](images/20220117-LocalTest-FindByLastName.png)

8. Test application function: List all Customer Accounts.

   ![Local Test: List all Customer Accounts](images/20220117-LocalTest-ListAllCustomerAccounts.png)

## References
1. [Java Microservice on Google Kubernetes Engine (GKE) Cluster](https://blog.adafycheng.dev/java-microservice-on-google-kubernetes-engine-gke-cluster)
2. [Testing in Spring Boot](https://www.baeldung.com/spring-boot-testing).
3. [Testing the Web Layer](https://spring.io/guides/gs/testing-web/).
