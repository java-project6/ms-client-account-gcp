# Use the official maven/Java 11 image to create a build artifact.
# https://hub.docker.com/_/maven
FROM maven:3.8-eclipse-temurin-11-alpine AS build-env

# Set the working directory to /app
WORKDIR /app
# Copy the pom.xml file to download dependencies
COPY pom.xml ./
# Copy local code to the container image.
COPY src ./src

# Download dependencies and build a release artifact.
RUN mvn package -DskipTests


FROM eclipse-temurin:11.0.14.1_1-jre-alpine

# Create a non-root group and a non-root user
RUN addgroup -S appgroup && adduser -S appuser -G appgroup

# Tell docker that all future commands should run as the non-root appuser user
USER appuser

WORKDIR /home/newuser

# Copy the jar to the production image from the builder stage.
COPY --from=build-env /app/target/ms.client.account.gcp-0.0.1-SNAPSHOT.jar /ms.client.account.gcp-0.0.1-SNAPSHOT.jar

# Run the web application on container startup.
CMD ["java", "-jar", "/ms.client.account.gcp-0.0.1-SNAPSHOT.jar"]
